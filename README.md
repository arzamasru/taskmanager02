# Task Manager 2

### Software requirements ###
java 1.8

### Technology stack ###
MAVEN, GITLAB, GIT-IGNORE, MANIFEST.MF, MAVEN-JAR-PLUGIN

### Developer ###
name: Lavrov Kirill

email: arzamasru@gmail.com

url: https://gitlab.com/arzamasru

organization: i-teco

organizationUrl: http://www.i-teco.ru

### Install ###
```
mvn install
```

### Run ###
```
java -jar Example.jar
```
